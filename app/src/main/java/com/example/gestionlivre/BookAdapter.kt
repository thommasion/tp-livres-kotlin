package com.example.gestionlivre

import android.content.Intent
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.annotation.RequiresApi
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerAdapter
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.booklist_main_item.view.*

class BookAdapter(options: FirestoreRecyclerOptions<Book>) :
    FirestoreRecyclerAdapter<Book, BookAdapter.BookViewHolder>(options) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        return BookViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.booklist_main_item, parent, false)
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    override fun onBindViewHolder(holder: BookViewHolder, position: Int, model: Book) {
        holder.name.text = model.Nom
        holder.author.text = model.Auteur
        if(model.image?.isNotEmpty()!!) {
            Picasso.get().load(model.image).resize(3000,2000).centerInside().into(holder.image)
        }
        var btnDetails : Button = holder.itemView.findViewById(R.id.bookdetail)
        btnDetails.setOnClickListener(View.OnClickListener() {
            val intent : Intent = Intent(holder.itemView.context, BookDetailActivity::class.java)
            intent.putExtra("id",model.id.toString())
            holder.itemView.context.startActivity(intent)
        })




    }

    class BookViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        var name = itemView.book_name
        var author = itemView.author_name
        var image = itemView.imageBook

    }

}