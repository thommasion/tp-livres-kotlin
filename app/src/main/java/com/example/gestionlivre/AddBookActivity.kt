package com.example.gestionlivre

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase

class AddBookActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.addbook)
        var nomTxt : EditText = findViewById(R.id.nomLivre)
        var auteurTxt : EditText = findViewById(R.id.Auteur)
        var descriptionTxt : EditText = findViewById(R.id.Description)
        var imageTxt : EditText = findViewById(R.id.Image)
        var validerBtn : Button = findViewById(R.id.validation)
        var retourBtn : Button = findViewById(R.id.Retour)


        validerBtn.setOnClickListener(View.OnClickListener {
            val nom = nomTxt.text.toString()
            val auteur = auteurTxt.text.toString()
            val description = descriptionTxt.text.toString()
            val image = imageTxt.text.toString()
            val isRead : Boolean = false
            val idUser : String? = Firebase.auth.uid

            if (idUser != null) {
                saveData(nom,auteur,description,image,isRead,idUser)
            }
        })
        retourBtn.setOnClickListener {
            startActivity(Intent(this, BookListActivity::class.java))
        }





    }

     fun saveData(nom : String, auteur : String, description : String, image : String, isRead : Boolean, idUser : String) {
         var db : FirebaseFirestore = FirebaseFirestore.getInstance()
         val data : MutableMap<String, Any> = HashMap()
         data["id"] = db.collection("book").document().id
         data["Nom"] = nom
         data["Auteur"] = auteur
         data["Resume"] = description
         data["image"] = image
         data["isReaded"] = isRead
         data["idUser"] = idUser
         db.collection("book").document(data["id"].toString()).set(data).addOnSuccessListener {
             Toast.makeText(baseContext,"Ajout du livre réussi ! ",Toast.LENGTH_SHORT).show()
             startActivity(Intent(this, BookListActivity::class.java))
         }
             .addOnFailureListener {
                 Toast.makeText(baseContext,"Erreur dans l'ajout ! ",Toast.LENGTH_SHORT).show()
             }
    }
}