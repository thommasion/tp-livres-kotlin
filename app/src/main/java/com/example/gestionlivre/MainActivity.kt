package com.example.gestionlivre

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.FirebaseUser
import com.google.firebase.auth.ktx.auth
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.activity_main.*
class MainActivity : AppCompatActivity() {
    private lateinit var email : EditText
    private lateinit var password : EditText
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        auth = Firebase.auth
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        email = findViewById(R.id.email)
        password = findViewById(R.id.password)
        btn1.setOnClickListener {
            // [START sign_in_with_email]
            if(email.text.trim().toString().isNullOrEmpty() or  password.text.toString().isNullOrEmpty()) {
                Toast.makeText(baseContext, "Email ou/et mot de passe non renseigné",Toast.LENGTH_SHORT).show()
            }
            else {
                auth.signInWithEmailAndPassword(email.text.trim().toString(), password.text.toString())
                    .addOnCompleteListener(this) { task ->
                        if (task.isSuccessful) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d(TAG, "signInWithEmail:success")
                            val user = auth.currentUser
                            updateUI(user)
                            Toast.makeText(baseContext, "Vous êtes connecté en tant que " + user.email,Toast.LENGTH_SHORT).show()
                            val intent : Intent = Intent(this, BookListActivity::class.java)
                            startActivity(intent)
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w(TAG, "signInWithEmail:failure", task.exception)
                            Toast.makeText(baseContext, "Email/Mot de passe incorrect. ",
                                Toast.LENGTH_LONG).show()
                            updateUI(null)
                        }
                    }
                // [END sign_in_with_email]
            }

        }

    }

    // [START on_start_check_user]
    public override fun onStart() {
        super.onStart()
        // Check if user is signed in (non-null) and update UI accordingly.
        val currentUser = auth.currentUser
        if(currentUser != null){
            reload();
        }
    }




    private fun updateUI(user: FirebaseUser?) {

    }


    private fun reload() {
    }

    companion object {

        private const val TAG = "EmailPassword"
    }
}