package com.example.gestionlivre

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.firebase.ui.firestore.FirestoreRecyclerOptions
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.auth.FirebaseAuth
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.CollectionReference
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.firestore.Query
import com.google.firebase.ktx.Firebase
import kotlinx.android.synthetic.main.booklist_main.*

class BookListActivity : AppCompatActivity() {
    private lateinit var mRecyclerView: RecyclerView
    private var db : FirebaseFirestore = FirebaseFirestore.getInstance()
    var bookAdapter : BookAdapter? = null
    var idUtilisateur : String? = null
    private lateinit var auth: FirebaseAuth
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.booklist_main)
        auth  = Firebase.auth
        idUtilisateur = auth.currentUser.uid
        setUpRecyclerView()
        val btnAdd : FloatingActionButton = findViewById(R.id.addBook)
        val btnDisconnect :Button = findViewById(R.id.disconnect)
        btnAdd.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, AddBookActivity::class.java))
        })
        btnDisconnect.setOnClickListener {
            Firebase.auth.signOut()
            startActivity(Intent(this, MainActivity::class.java))
        }


    }


     fun setUpRecyclerView() {
         val collectionReference : CollectionReference = db.collection("book")
         val query : Query = collectionReference.whereEqualTo("idUser",idUtilisateur)
         val firestoreRecyclerOptions : FirestoreRecyclerOptions<Book> = FirestoreRecyclerOptions.Builder<Book>()
           .setQuery(query, Book::class.java)
           .build()
         bookAdapter = BookAdapter(firestoreRecyclerOptions)
         grouplist_recyler_view.layoutManager = LinearLayoutManager(this)
         grouplist_recyler_view.adapter = bookAdapter
     }

    override fun onStart() {
        super.onStart()
        bookAdapter!!.startListening()
    }

    override fun onDestroy() {
        super.onDestroy()
        bookAdapter!!.stopListening()
    }


}













