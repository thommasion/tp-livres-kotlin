package com.example.gestionlivre

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.firebase.auth.ktx.auth
import com.google.firebase.firestore.FirebaseFirestore
import com.google.firebase.ktx.Firebase
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.bookdetails.*


class BookDetailActivity : AppCompatActivity() {

    private var db: FirebaseFirestore = FirebaseFirestore.getInstance()

    @SuppressLint("SetTextI18n")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.bookdetails)

        var isReaded: CheckBox = findViewById(R.id.isReaded)
        var etTitre: EditText = findViewById(R.id.titreFilm)
        var etAuteur: EditText = findViewById(R.id.nomAuteur)
        var etDescription: EditText = findViewById(R.id.description)
        val etIrlImageLivre : EditText = findViewById(R.id.irlImageLivre)
        var image: ImageView = findViewById(R.id.imgLivre)
        var btnRetour: Button = findViewById(R.id.btnRetour)
        val bookId: String? = intent.getStringExtra("id")


        val collectionReference =
            db.collection("book").whereEqualTo("id", bookId)
                .get()
                .addOnSuccessListener {
                    val document = it.first()
                    document.data["Auteur"]?.let { auteurStr ->
                        auteurStr as String

                        etAuteur.setText(auteurStr)
                    }
                    document.data["Nom"]?.let { nomStr ->
                        nomStr as String

                        etTitre.setText(nomStr)
                    }

                    document.data["image"]?.let { imgStr ->
                        imgStr as String
                        if (imgStr.isNotEmpty()) {
                            etIrlImageLivre.setText(imgStr)
                            Picasso.get().load(imgStr).resize(3000, 2000).centerInside().into(image)
                        }
                    }

                    document.data["Resume"]?.let { resumeStr ->
                        resumeStr as String

                        etDescription.setText(resumeStr)
                    }

                    document.data["isReaded"]?.let { readedBool ->
                        readedBool as Boolean

                        isReaded.isChecked = readedBool
                    }

                }

        btnRetour.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, BookListActivity::class.java))
        })
        btnSaveLu.setOnClickListener {
            val readed: Boolean = isReaded.isChecked
            val nom = etTitre.text.toString()
            val auteur = etAuteur.text.toString()
            val description = etDescription.text.toString()
            val irlImage = etIrlImageLivre.text.toString()
                bookId?.let {
                    db.collection("book").document(bookId).update("isReaded", readed,"Nom",nom,"Auteur",auteur,"Resume",description,"image",irlImage)
                        .addOnSuccessListener {
                            Toast.makeText(baseContext, "Modification réussi ! ", Toast.LENGTH_SHORT)
                                .show()
                        }
                        .addOnFailureListener {
                            Toast.makeText(
                                baseContext,
                                "Erreur dans la modification ! ",
                                Toast.LENGTH_SHORT
                            ).show()
                        }
                }
        }

    }
}




